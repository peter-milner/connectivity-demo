import sys
import grequests
from xml.etree import ElementTree as ET
from collections import Counter
from pymongo import MongoClient

client = MongoClient()
db = client.test_database
db['hotels'].drop()
collection = db['hotels']

JSON_ENDPOINT = 'https://experimentation.getsnaptravel.com/interview/hotels'
XML_ENDPOINT = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'

city = sys.argv[1]
checkin = sys.argv[2]
checkout = sys.argv[3]

xml_body = '''
    <?xml version="1.0" encoding="UTF-8"?>
    <root>
        <checkin>{}</checkin>
        <checkout>{}</checkout>
        <city>{}</city>
        <provider>snaptravel</provider>
    </root>
'''.format(city, checkin, checkout)

responses = grequests.map([
    grequests.post(
        JSON_ENDPOINT,
        data={
            'city': city,
            'checkin': checkin,
            'checkout': checkout,
            'provider': 'snaptravel'
        }
    ),
    grequests.post(
        XML_ENDPOINT,
        data=xml_body,
        headers={
            'Content-Type': 'text/xml'
        }
    )
])

json_cnt = Counter()
for h in responses[0].json()['hotels']:
    json_cnt[h['id']]=1

xml_cnt = Counter()
root = ET.fromstring(responses[1].content)
for c in root:
    xml_cnt[int(c[0].text)]=1

intersection_ids = (json_cnt & xml_cnt).keys()

for h in responses[0].json()['hotels']:
    if h['id'] in intersection_ids:
        h['_id'] = h['id']
        del h['id']
        collection.insert_one(h)

for h in collection.find():
    print(h)
